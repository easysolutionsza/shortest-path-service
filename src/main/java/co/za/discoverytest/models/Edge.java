package co.za.discoverytest.models;
import lombok.Data;

@Data
public class Edge  {

    private final int id;
    private final Vertex source;
    private final Vertex destination;
    private final int weight;

}