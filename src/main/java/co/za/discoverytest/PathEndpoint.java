package co.za.discoverytest;

import com.discoverytest.types.GetShortestPathRequest;
import com.discoverytest.types.GetShortestPathResponse;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

@Endpoint
public class PathEndpoint {
	private static final String NAMESPACE_URI = "http://www.discoverytest.com/xml/path";

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "GetShortestPathRequest")
	@ResponsePayload
	public GetShortestPathResponse getPath(@RequestPayload GetShortestPathRequest request) {

		GetShortestPathResponse response = new GetShortestPathResponse();

		return response;
	}
}